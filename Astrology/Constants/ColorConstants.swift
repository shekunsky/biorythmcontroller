//
//  ColorConstants.swift
//  Astrology
//
//  Created by Alex2 on 04.01.2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

struct ColorConstants {
    
    static let kAstrologyFioletColor        =  UIColor(red:160.0/255, green:160.0/255, blue:230.0/255, alpha:1)
    static let kAstrologyFioletLightColor   =  UIColor(red:215.0/255, green:215.0/255, blue:240.0/255, alpha:1)
    static let kAstrologyFioletDarkColor    =  UIColor(red:124.0/255, green:124.0/255, blue:155.0/255, alpha:1)
    static let kAstrologyZodiakBgColor      =  UIColor(red:100.0/255, green:100.0/255, blue:150.0/255, alpha:1)
    static let kAstrologyRedColor           =  UIColor(red:254.0/255, green:95.0/255, blue:95.0/255, alpha:1)
    static let kAstrologyChatDarkColor      =  UIColor(red:54.0/255, green:52.0/255, blue:72.0/255, alpha:1)
    static let kAstrologyHeaderGradient1    =  UIColor(red:77.0/255, green:26.0/255, blue:233.0/255, alpha:1)
    static let kAstrologyHeaderGradient2    =  UIColor(red:137.0/255, green:64.0/255, blue:255.0/255, alpha:1)
    static let kAstrologyBiorythmEmotion    =  UIColor(red:255/255.0, green:212/255.0, blue:0/255.0 , alpha: 1.0)
    static let kAstrologyBiorythmPhysical   =  UIColor(red:255/255.0, green:94/255.0, blue:94/255.0 , alpha: 1.0)
    static let kAstrologyBiorythmSpyrital   =  UIColor(red:49/255.0, green:206/255.0, blue:255/255.0 , alpha: 1.0)
}

