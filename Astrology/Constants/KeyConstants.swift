//
//  KeyConstants.swift
//  Astrology
//
//  Created by Alex2 on 04.01.2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

struct KeyConstants {
    
    struct Numbers {
        static let numberOfDaysForBiorythm  = 7
    }

    struct UserDefaults {
        static let kUserDateOfBirth         = "23-03-1977"
    }
    
}
