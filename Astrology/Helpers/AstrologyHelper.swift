//
//  AstrologyHelper.swift
//  Astrology
//
//  Created by Alex2 on 11.01.2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class AstrologyHelper {
    
    class func addGradientTo(view: UIView) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [ColorConstants.kAstrologyHeaderGradient1.cgColor, ColorConstants.kAstrologyHeaderGradient2.cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: view.frame.size.width, height: view.frame.size.height)
    }
    
    class func numberOfUserLiveDays() -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        let start = dateFormatter.date(from: KeyConstants.UserDefaults.kUserDateOfBirth)
        let end  = Date()
        
        return Calendar.current.dateComponents([.day], from: start ?? end, to: end).day!
    }
    
}

