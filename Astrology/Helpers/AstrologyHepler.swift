//
//  AstrologyHepler.swift
//  Astrology
//
//  Created by Alex2 on 11.01.2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

final class AstrologyHelper {
    
    // Can't init is singleton
    private init() { }
    
    // MARK: Shared Instance
    static let shared = AstrologyHelper()
    
    // MARK: Local Variable
    
    func imageForZodiak(zodiak:Zodiac) -> UIImage{
        switch zodiak {
        case .Aquarius:
            return UIImage(named: "Zodiak_Aquarius")!
        case .Aries:
            return UIImage(named: "Zodiak_Aries")!
        case .Cancer:
            return UIImage(named: "Zodiak_Cancer")!
        case .Capricorn:
            return UIImage(named: "Zodiak_Capricorn")!
        case .Gemini:
            return UIImage(named: "Zodiak_Gemini")!
        case .Leo:
            return UIImage(named: "Zodiak_Leo")!
        case .Libra:
            return UIImage(named: "Zodiak_Libra")!
        case .Pisces:
            return UIImage(named: "Zodiak_Pisces")!
        case .Sagittarius:
            return UIImage(named: "Zodiak_Sagittarius")!
        case .Scorpio:
            return UIImage(named: "Zodiak_Scorpio")!
        case .Taurus:
            return UIImage(named: "Zodiak_Taurus")!
        case .Virgo:
            return UIImage(named: "Zodiak_Virgo")!
            
        default:
            return UIImage()
        }
    }
    
    func imageForIndex(index:Int) -> UIImage{
        switch index {
        case 10:
            return UIImage(named: "Zodiak_Aquarius")!
        case 0:
            return UIImage(named: "Zodiak_Aries")!
        case 3:
            return UIImage(named: "Zodiak_Cancer")!
        case 9:
            return UIImage(named: "Zodiak_Capricorn")!
        case 2:
            return UIImage(named: "Zodiak_Gemini")!
        case 4:
            return UIImage(named: "Zodiak_Leo")!
        case 6:
            return UIImage(named: "Zodiak_Libra")!
        case 11:
            return UIImage(named: "Zodiak_Pisces")!
        case 8:
            return UIImage(named: "Zodiak_Sagittarius")!
        case 7:
            return UIImage(named: "Zodiak_Scorpio")!
        case 1:
            return UIImage(named: "Zodiak_Taurus")!
        case 5:
            return UIImage(named: "Zodiak_Virgo")!
            
        default:
            return UIImage()
        }
    }
    
    func zodiakForIndex(index:Int) -> Zodiac{
        switch index {
        case 10:
            return .Aquarius
        case 0:
            return .Aries
        case 3:
            return .Cancer
        case 9:
            return .Capricorn
        case 2:
            return .Gemini
        case 4:
            return .Leo
        case 6:
            return .Libra
        case 11:
            return .Pisces
        case 8:
            return .Sagittarius
        case 7:
            return .Scorpio
        case 1:
            return .Taurus
        case 5:
            return .Virgo
            
        default:
            return .Undefined
        }
    }
    
    func indexOfZodiac(zodiac:Zodiac) -> Int{
        switch zodiac {
        case .Aquarius:
            return 10
        case .Aries:
            return 0
        case .Cancer:
            return 3
        case .Capricorn:
            return 9
        case .Gemini:
            return 2
        case .Leo:
            return 4
        case .Libra:
            return 6
        case .Pisces:
            return 11
        case .Sagittarius:
            return 8
        case .Scorpio:
            return 7
        case .Taurus:
            return 1
        case .Virgo:
            return 5
            
        default:
            return 0
        }
    }
    
    func userZodiakFromDefaults() -> Zodiac{
        if let zodiakRawValue = UserDefaults.standard.value(forKey: KeyConstants.Key.UserDefaults.kUserZodiak) as? String {
            return Zodiac(rawValue: zodiakRawValue)!
        }
        return Zodiac.Aries
    }
    
    func friendZodiakFromDefaults() -> Zodiac{
        if let zodiakRawValue = UserDefaults.standard.value(forKey: KeyConstants.Key.UserDefaults.kFriendZodiak) as? String {
            return Zodiac(rawValue: zodiakRawValue)!
        }
        return Zodiac.Aries
    }
    
    func datePeriodForZodiak(zodiak:Zodiac) -> String{
        switch zodiak {
        case .Aquarius:
            return NSLocalizedString("Zodiak.Period.Aquarius", comment: "")
        case .Aries:
            return NSLocalizedString("Zodiak.Period.Aries", comment: "")
        case .Cancer:
            return NSLocalizedString("Zodiak.Period.Cancer", comment: "")
        case .Capricorn:
            return NSLocalizedString("Zodiak.Period.Capricorn", comment: "")
        case .Gemini:
            return NSLocalizedString("Zodiak.Period.Gemini", comment: "")
        case .Leo:
            return NSLocalizedString("Zodiak.Period.Leo", comment: "")
        case .Libra:
            return NSLocalizedString("Zodiak.Period.Libra", comment: "")
        case .Pisces:
            return NSLocalizedString("Zodiak.Period.Pisces", comment: "")
        case .Sagittarius:
            return NSLocalizedString("Zodiak.Period.Sagittarius", comment: "")
        case .Scorpio:
            return NSLocalizedString("Zodiak.Period.Scorpio", comment: "")
        case .Taurus:
            return NSLocalizedString("Zodiak.Period.Taurus", comment: "")
        case .Virgo:
            return NSLocalizedString("Zodiak.Period.Virgo", comment: "")
            
        default:
            return ""
        }
    }
    
    class func addGradientTo(view: UIView) {
        DispatchQueue.main.async() {
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.colors = [ColorConstants.kAstrologyHeaderGradient1.cgColor, ColorConstants.kAstrologyHeaderGradient2.cgColor]
            gradient.locations = [0.0 , 1.0]
            gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
            gradient.frame = CGRect(x: 0.0, y: 0.0, width: view.frame.size.width, height: view.frame.size.height)
            if (view is UIButton) && (view.tag == 4321) {
                view.layer.insertSublayer(gradient, at: 1)
            } else {
                view.layer.insertSublayer(gradient, at: 0)
            }
        }
    }
    
    class func appVersion()->String {
        let nsObject: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
        let version = nsObject as! String
        let buildObject :  AnyObject? = Bundle.main.infoDictionary?["CFBundleVersion"] as AnyObject
        let buildString = buildObject as! String
        return version //+ "(\(buildString))"
    }
    
    class func modelIdentifier() -> String {
        if let simulatorModelIdentifier = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] { return simulatorModelIdentifier }
        var sysinfo = utsname()
        uname(&sysinfo) // ignore return value
        return String(bytes: Data(bytes: &sysinfo.machine, count: Int(_SYS_NAMELEN)), encoding: .ascii)!.trimmingCharacters(in: .controlCharacters)
    }
    
    class func appSystemInfo()->String {
        let os = "Операционная система: IOS \(ProcessInfo().operatingSystemVersion)\n"
        let appVersion = "Версия приложения: " + AstrologyHelper.appVersion() + "\n"
        let deviceModel = "Устройство: " + AstrologyHelper.modelIdentifier() + "\n"
        return os + appVersion + deviceModel
    }
}
