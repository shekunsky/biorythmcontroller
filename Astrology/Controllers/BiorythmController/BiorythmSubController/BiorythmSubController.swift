//
//  BiorythmSubController.swift
//  Astrology
//
//  Created by Alex2 on 11/6/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import Charts
import EasyTipView

class BiorythmSubController: UIViewController  {
    
    //MARK: - Outlets
    @IBOutlet weak var scrollView       : UIScrollView!
    @IBOutlet weak var mainView         : UIView!
    @IBOutlet weak var topView          : UIView!
    @IBOutlet weak var graphView        : UIView!
    @IBOutlet weak var daysCountLabel   : UILabel!
    @IBOutlet weak var physicalLabel    : UILabel!
    @IBOutlet weak var emotionalLabel   : UILabel!
    @IBOutlet weak var spyritalLabel    : UILabel!
    @IBOutlet weak var chartView        : LineChartView!
    @IBOutlet weak var graphLegendLabel : UILabel!
    @IBOutlet weak var tableView        : UITableView!
    
    //MARK: - Vars
    var currentIndex = 0;
    var tipView: EasyTipView?
    let tableCellHeight: CGFloat = 170.0
    lazy var viewModel: BiorythmViewModel = {
        return BiorythmViewModel()
    }()
    
    //Mark: - LifeCicle
    init(index: Int) {
        super.init(nibName: nil, bundle: nil)
        self.currentIndex = index
        switch currentIndex {
        case 0:
            title = NSLocalizedString("Biorythm.Menu.Today", comment: "")
        case 1:
            title = NSLocalizedString("Biorythm.Menu.Tomorrow", comment: "")
        default:
            title = NSLocalizedString("Biorythm.Menu.Today", comment: "")
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Init the static view
        initView()
        
        // Init view model
        initVM()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func initView() {
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        navigationController?.setNavigationBarHidden(true, animated: false)
        AstrologyHelper.addGradientTo(view: self.view)
        
        if self.currentIndex == 0 {
            daysCountLabel.text = String(format: NSLocalizedString("Вы прожили %@ дней", comment: ""), "\(AstrologyHelper.numberOfUserLiveDays()+self.currentIndex)")
        } else {
            daysCountLabel.text = String(format: NSLocalizedString("Вы проживете %@ дней", comment: ""), "\(AstrologyHelper.numberOfUserLiveDays()+self.currentIndex)")
        }
        graphLegendLabel.text = String(format: NSLocalizedString("Ваши биоритмы на ближайшие %@ дней", comment: ""), "\(KeyConstants.Numbers.numberOfDaysForBiorythm)")
        physicalLabel.text = NSLocalizedString("Физический", comment: "")
        emotionalLabel.text = NSLocalizedString("Эмоциональный", comment: "")
        spyritalLabel.text = NSLocalizedString("Духовный", comment: "")
        
        //tableView
        self.tableView.register(UINib(nibName: "BiorythmTableCell", bundle: nil), forCellReuseIdentifier: "BiorythmTableCell")
        tableView.estimatedRowHeight = tableCellHeight
        tableView.rowHeight = tableCellHeight
    }
    
    //MARK: - CHART setup functions
    private func initChartViewWithModel(chartModel: BiorythmChartModel) {
        // Do any additional setup after loading the view.
        chartView.chartDescription?.enabled = false
        chartView.dragEnabled = false
        chartView.setScaleEnabled(false)
        chartView.pinchZoomEnabled = false
        chartView.legend.enabled = false
        
        //detect tap
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(chartTapped))
        chartView.addGestureRecognizer(tapRecognizer)
    
        let xAxis = chartView.xAxis
        setupXAxis(xAxis)
        
        let leftAxis = chartView.leftAxis
        setupLeftAxis(leftAxis)

        let rightAxis = chartView.rightAxis
        setupRightAxis(rightAxis)
        
        var yVals1 : [ChartDataEntry] = []
        for i in 0...KeyConstants.Numbers.numberOfDaysForBiorythm-1 {
            yVals1.append(ChartDataEntry(x: Double(chartModel.days[i]), y: Double(chartModel.emotional[i])))
        }
        let set1 = LineChartDataSet(values: yVals1, label: "Эмоциональный")
        setupEmotionalSet(set1)
        
        var yVals2 : [ChartDataEntry] = []
        for i in 0...KeyConstants.Numbers.numberOfDaysForBiorythm-1 {
            yVals2.append(ChartDataEntry(x: Double(chartModel.days[i]), y: Double(chartModel.spyrital[i])))
        }
        let set2 = LineChartDataSet(values: yVals2, label: "Духовный")
        setupSpyritalSet(set2)
        
        var yVals3 : [ChartDataEntry] = []
        for i in 0...KeyConstants.Numbers.numberOfDaysForBiorythm-1 {
            yVals3.append(ChartDataEntry(x: Double(chartModel.days[i]), y: Double(chartModel.physical[i])))
        }
        let set3 = LineChartDataSet(values: yVals3, label: "Физический")
        setupPhysicalSet(set3)
        
        let data = LineChartData(dataSets: [set1, set2, set3])
        data.setValueTextColor(ColorConstants.kAstrologyHeaderGradient2)
        data.setValueFont(UIFont.systemFont(ofSize: 10, weight: .medium))
        
        chartView.data = data
        chartView.setExtraOffsets(left: 0, top: 20, right: 0, bottom: 0)
        
        if (currentIndex == 0) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                self.showHelpTips()
            })
            DispatchQueue.main.asyncAfter(deadline: .now() + 7.0, execute: {
                self.tipView?.dismiss()
            })
        }
        chartView.extraRightOffset = 15
    }
    
    private func commonInitFor(set: LineChartDataSet) {
        set.axisDependency = .left
        set.setCircleColor(.black)
        set.lineWidth = 2
        set.circleRadius = 3
        set.drawCircleHoleEnabled = false
        set.drawValuesEnabled = false
        set.mode = .cubicBezier
    }
    
    private func setupEmotionalSet(_ set: LineChartDataSet) {
        commonInitFor(set: set)
        set.setColor(ColorConstants.kAstrologyBiorythmEmotion)
        set.fillColor = ColorConstants.kAstrologyBiorythmEmotion
        set.highlightColor = ColorConstants.kAstrologyBiorythmEmotion
        set.drawValuesEnabled = true
    }
    
    private func setupSpyritalSet(_ set: LineChartDataSet) {
        commonInitFor(set: set)
        set.setColor(ColorConstants.kAstrologyBiorythmSpyrital)
        set.fillColor = ColorConstants.kAstrologyBiorythmSpyrital
        set.highlightColor = ColorConstants.kAstrologyBiorythmSpyrital
    }
    
    private func setupPhysicalSet(_ set: LineChartDataSet) {
        commonInitFor(set: set)
        set.setColor(ColorConstants.kAstrologyBiorythmPhysical)
        set.fillColor = ColorConstants.kAstrologyBiorythmPhysical
        set.highlightColor = ColorConstants.kAstrologyBiorythmPhysical
    }
    
    private func setupXAxis(_ xAxis: XAxis) {
        xAxis.labelTextColor = .gray
        xAxis.labelFont = UIFont.systemFont(ofSize: 14, weight: .medium)
        xAxis.drawAxisLineEnabled = false
        xAxis.labelPosition = .bottom
        xAxis.granularityEnabled = true
        xAxis.granularity = 1
        xAxis.valueFormatter = DefaultAxisValueFormatter(block: {(index, _) in
            return self.viewModel.modelForChart.dates[Int(index)]
        })
        xAxis.yOffset = 10
    }
    
    private func setupLeftAxis(_ leftAxis: YAxis) {
        leftAxis.labelTextColor = .gray
        leftAxis.zeroLineColor = .clear
        leftAxis.axisLineColor = .clear
        leftAxis.drawZeroLineEnabled = false
        leftAxis.labelFont = UIFont.systemFont(ofSize: 13, weight: .medium)
        leftAxis.axisMaximum = 100
        leftAxis.axisMinimum = -100
        leftAxis.drawGridLinesEnabled = false
        leftAxis.granularityEnabled = false
        leftAxis.spaceBottom = 25.0
    }
    
    private func setupRightAxis(_ rightAxis: YAxis) {
        rightAxis.labelTextColor = .clear
        rightAxis.axisMaximum = 100
        rightAxis.axisMinimum = -100
        rightAxis.granularityEnabled = false
        rightAxis.labelTextColor = .clear
        rightAxis.drawLabelsEnabled = false
    }
    
    //MARK: - Tap on Chart
    @objc private func chartTapped(_ sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            let position = sender.location(in: chartView)
            let dataSet = chartView.getDataSetByTouchPoint(point: position)
            
            let state = (dataSet?.drawValuesEnabled)!
            chartView.data?.dataSets.forEach{ $0.drawValuesEnabled = false }
            dataSet?.drawValuesEnabled = !state
            if tipView != nil {
                tipView?.dismiss()
            }
        }
    }
    
    private func initVM() {
        
        //Bindings
        viewModel.updateChartClosure = { [weak self]  in
            DispatchQueue.main.async {
                if let chartModel = self?.viewModel.modelForChart {
                    self?.initChartViewWithModel(chartModel: chartModel)
                    self?.tableView.reloadData()
                }
            }
        }
        
        // prepare data for charts
        viewModel.initFetchFor(index: currentIndex)
    }

    private func showHelpTips() {
        var preferences = EasyTipView.Preferences()
        preferences.drawing.font = .systemFont(ofSize: 14)
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.backgroundColor = ColorConstants.kAstrologyHeaderGradient1
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
        preferences.animating.showDuration = 2.0
        preferences.animating.dismissDuration = 2.0
        EasyTipView.globalPreferences = preferences
        
        tipView = EasyTipView(text: NSLocalizedString("Для отображения значений на графике сделайте двойной тап по интересующей кривой.", comment: ""),
                              preferences: preferences,
                              delegate: self)
        tipView!.show(forView: chartView, withinSuperview: scrollView)
    }
    
}

//MARK: - UIScrollViewDelegate
extension BiorythmSubController: UIScrollViewDelegate {
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        tipView?.dismiss()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        tipView?.dismiss()
    }
}

//MARK: - EasyTipViewDelegate
extension BiorythmSubController: EasyTipViewDelegate {
    func easyTipViewDidDismiss(_ tipView : EasyTipView) {
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension BiorythmSubController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BiorythmTableCell", for: indexPath) as? BiorythmTableCell else {
            fatalError("Cell not exists")
        }
        
        cell.initCelWith(index: indexPath.row, chartModel: viewModel.modelForChart)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableCellHeight
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
    
}
