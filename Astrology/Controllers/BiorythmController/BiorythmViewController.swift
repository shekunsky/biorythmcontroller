//
//  BiorythmViewController.swift
//  Astrology
//
//  Created by Alex2 on 11/6/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import Parchment

class BiorythmViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var backButton               : UIButton!
    @IBOutlet weak var backButtonTopConstraint  : NSLayoutConstraint!
    @IBOutlet weak var headerTitleLabel         : UILabel!
    @IBOutlet weak var mainView                 : UIView!
    
    
    //Mark: - LifeCicle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Init the static view
        initView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func initView() {
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        navigationController?.setNavigationBarHidden(true, animated: false)
        AstrologyHelper.addGradientTo(view: self.view)
        headerTitleLabel.text = NSLocalizedString("Biorythms", comment: "")
        
        //Paging menu
        let viewControllers = [BiorythmSubController(index: 0), BiorythmSubController(index: 1)]
        let pagingViewController = FixedPagingViewController(viewControllers: viewControllers)
        pagingViewController.menuBackgroundColor = .clear
        pagingViewController.indicatorColor = .white
        pagingViewController.textColor = .white
        pagingViewController.selectedTextColor = .white
        pagingViewController.selectedFont = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
        
        addChild(pagingViewController)
        mainView.addSubview(pagingViewController.view)
        mainView.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
        
        //iPhoneX/Xr/XsMax
        if UIScreen.main.nativeBounds.height == 2436  ||
            UIScreen.main.nativeBounds.height == 1792 ||
            UIScreen.main.nativeBounds.height == 2688 {
            backButtonTopConstraint.constant = 40
        }
        else{
            backButtonTopConstraint.constant = 30
        }
    }
    
    
    //Mark: - Actions
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
