//
//  BiorythmViewModel.swift
//  Astrology
//
//  Created by Alex2 on 11/6/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

struct BiorythmChartModel {
    var days        : [Int]
    var dates       : [String]
    var emotional   : [Float]
    var physical    : [Float]
    var spyrital    : [Float]
}

class BiorythmViewModel {
    
    var modelForChart: BiorythmChartModel = BiorythmChartModel(days:[], dates:[], emotional: [], physical: [], spyrital: []) {
        didSet {
            self.updateChartClosure?()
        }
    }
    
    var updateChartClosure: (()->())?

    func initFetchFor(index: Int) {
        var numberOfLivedDays = AstrologyHelper.numberOfUserLiveDays()
        var startDate = Date()
        if index == 1 {
            startDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
            numberOfLivedDays = numberOfLivedDays + 1
        }
        var weakModel : BiorythmChartModel = BiorythmChartModel(days:[], dates:[], emotional: [], physical: [], spyrital: [])
        
        for i in  1...KeyConstants.Numbers.numberOfDaysForBiorythm {
            let dayIndex = Calendar.current.dateComponents([.day], from: startDate).day
            let month = Calendar.current.dateComponents([.month], from: startDate).month

            weakModel.days.append(i-1)
            weakModel.dates.append("\(dayIndex!).\(month!)")
            weakModel.emotional.append(100 * sin(2 * 3.14 * Float(numberOfLivedDays+i-1) / 28.0))
            weakModel.physical.append(100 * sin(2 * 3.14 * Float(numberOfLivedDays+i-1) / 23.0))
            weakModel.spyrital.append(100 * sin(2 * 3.14 * Float(numberOfLivedDays+i-1) / 53.0))
            
            startDate = Calendar.current.date(byAdding: .day, value: 1, to:startDate)!
            
        }
        modelForChart = weakModel
    }
}
