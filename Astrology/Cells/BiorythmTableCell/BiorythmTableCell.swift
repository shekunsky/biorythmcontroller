//
//  BiorythmTableCell.swift
//  Astrology
//
//  Created by Alex2 on 11/7/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import UICircularProgressRing

class BiorythmTableCell: UITableViewCell {

    @IBOutlet weak var circularProgressView : UICircularProgressRing!
    @IBOutlet weak var percentLabel         : UILabel!
    @IBOutlet weak var growthLabel          : UILabel!
    @IBOutlet weak var growthImageView      : UIImageView!
    @IBOutlet weak var graphImageView       : UIImageView!
    @IBOutlet weak var titleLabel           : UILabel!
    @IBOutlet weak var subtitleLabel        : UILabel!
    @IBOutlet weak var descriptionLabel     : UILabel!
    @IBOutlet weak var lineImageView        : UIImageView!
    @IBOutlet weak var zeroLineConstraint   : NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func initCelWith(index: Int, chartModel: BiorythmChartModel) {
        circularProgressView.shouldShowValueText = false
        circularProgressView.backgroundColor = .clear
        circularProgressView.outerRingColor = .groupTableViewBackground
        circularProgressView.outerRingWidth = 4
        circularProgressView.innerRingWidth = 4
        circularProgressView.maxValue = 100
        circularProgressView.startAngle = 270
        var start: Float = 0.0
        var end: Float = 0.0
        var currentColor : UIColor = .clear
        switch index {
        case 0:
            setupPhysicalCell(start: &start, end: &end, currentColor: &currentColor, chartModel: chartModel)
        case 1:
            setupEmotionalCell(start: &start, end: &end, currentColor: &currentColor, chartModel: chartModel)
        case 2:
            setupSpyritalCell(start: &start, end: &end, currentColor: &currentColor, chartModel: chartModel)
        default:
            print("default")
        }
        
        var x = abs (end - start)
        self.circularProgressView.value = CGFloat(x)
        //detect grow or not
        if end < start {
            x = -x
            growthImageView.image = UIImage(named: "ic_recession")
            growthLabel.text = NSLocalizedString("Спад", comment: "")
            circularProgressView.isClockwise = false
        } else {
            growthImageView.image = UIImage(named: "ic_growth")
            growthLabel.text = NSLocalizedString("Рост", comment: "")
            circularProgressView.isClockwise = true
        }
        let angel = -(Double(x) * Double.pi) / 180.0
        percentLabel.text = "\(Int(x))%"
        percentLabel.textColor = currentColor
        circularProgressView.innerRingColor = currentColor
        lineImageView.transform = lineImageView.transform.rotated(by: CGFloat(angel))
        if (start < 0 && end < 0) {
            zeroLineConstraint.constant = -50
        }
        if (start > 0 && end > 0) {
            zeroLineConstraint.constant = 50
        }
    }
    
    private func setupPhysicalCell(start: inout Float, end: inout Float, currentColor: inout UIColor, chartModel: BiorythmChartModel) {
        titleLabel.text = NSLocalizedString("Физический", comment: "")
        subtitleLabel.text = NSLocalizedString("1-ая чакра Муладхара", comment: "")
        descriptionLabel.text = NSLocalizedString("Физический цикл определяет энергию человека, его силу, выносливость, координацию движения.", comment: "")
        graphImageView.image = UIImage(named: "physical_graph")
        lineImageView.image = UIImage(named: "line_physical")
        start = chartModel.physical[0]
        end = chartModel.physical[1]
        currentColor = ColorConstants.kAstrologyBiorythmPhysical
    }
    
    private func setupEmotionalCell(start: inout Float, end: inout Float, currentColor: inout UIColor, chartModel: BiorythmChartModel) {
        titleLabel.text = NSLocalizedString("Эмоциональный", comment: "")
        subtitleLabel.text = NSLocalizedString("2-ая чакра Свадхистана", comment: "")
        descriptionLabel.text = NSLocalizedString("Эмоциональный цикл обусловливает состояние нервной системы и настроение.", comment: "")
        graphImageView.image = UIImage(named: "emotional_graph")
        lineImageView.image = UIImage(named: "line_emotional")
        start = chartModel.emotional[0]
        end = chartModel.emotional[1]
        currentColor = ColorConstants.kAstrologyBiorythmEmotion
    }
    
    private func setupSpyritalCell(start: inout Float, end: inout Float, currentColor: inout UIColor, chartModel: BiorythmChartModel) {
        titleLabel.text = NSLocalizedString("Духовный", comment: "")
        subtitleLabel.text = NSLocalizedString("3-я чакра Манипура", comment: "")
        descriptionLabel.text = NSLocalizedString("Духовный определяет творческую способность личности.", comment: "")
        graphImageView.image = UIImage(named: "spiritual_graph")
        lineImageView.image = UIImage(named: "line_spiritual")
        start = chartModel.spyrital[0]
        end = chartModel.spyrital[1]
        currentColor = ColorConstants.kAstrologyBiorythmSpyrital
    }
}
