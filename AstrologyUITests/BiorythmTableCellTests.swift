//
//  BiorythmTableCellTests.swift
//  AstrologyUITests
//
//  Created by Alex2 on 11/9/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import XCTest
@testable import Astrology

class BiorythmTableCellTests: XCTestCase {
    
    var sut: BiorythmTableCell {
        let tableView  = UITableView()
        let bundle = Bundle(for: type(of: self))
        tableView.register(UINib(nibName: "BiorythmTableCell", bundle:bundle), forCellReuseIdentifier: "BiorythmTableCell")
        let indexPath = IndexPath(row: 0, section: 0)
        let localSut: BiorythmTableCell = (tableView.dequeueReusableCell(withIdentifier: "BiorythmTableCell", for: indexPath) as? BiorythmTableCell)!
        return localSut
    }
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        XCUIApplication().launch()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGrowthIndicationsInCell() {
        let mockedModelForChart = BiorythmChartModel(days: [1,2,3,4,5,6,7],
                                                     dates: ["1.11", "2.11", "3.11", "4.11", "5.11", "6.11", "7.11"],
                                                     emotional: [10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0],
                                                     physical: [11.0, 21.0, 31.0, 41.0, 51.0, 61.0, 71.0],
                                                     spyrital: [12.0, 22.0, 32.0, 42.0, 52.0, 62.0, 72.0])
        
        let sut = self.sut
        for i in  0...2 {
            sut.initCelWith(index: i, chartModel: mockedModelForChart)
            XCTAssertEqual(sut.growthLabel.text, NSLocalizedString("Рост", comment: ""))
            XCTAssertEqual(sut.growthImageView.image, UIImage(named: "ic_growth"))
        }
        
    }
    
    func testRecessionIndicationsInCell() {
        let mockedModelForChart = BiorythmChartModel(days: [1,2,3,4,5,6,7],
                                                     dates: ["1.11", "2.11", "3.11", "4.11", "5.11", "6.11", "7.11"],
                                                     emotional: [70.0, 60.0, 50.0, 40.0, 30.0, 20.0, 10.0],
                                                     physical: [71.0, 61.0, 51.0, 41.0, 31.0, 21.0, 11.0],
                                                     spyrital: [72.0, 62.0, 52.0, 42.0, 32.0, 22.0, 12.0])
        
        let sut = self.sut
        for i in  0...2 {
            sut.initCelWith(index: i, chartModel: mockedModelForChart)
            XCTAssertEqual(sut.growthLabel.text, NSLocalizedString("Спад", comment: ""))
            XCTAssertEqual(sut.growthImageView.image, UIImage(named: "ic_recession"))
        }
    }
    
}

