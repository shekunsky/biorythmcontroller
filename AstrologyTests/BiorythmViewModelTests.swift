//
//  BiorythmViewModelTests.swift
//  AstrologyTests
//
//  Created by Alex2 on 11/9/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import XCTest
@testable import Astrology

class BiorythmViewModelTests: XCTestCase {
    
    var sut: BiorythmViewModel!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFetchingDataForTodayCharts() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        sut = BiorythmViewModel()
        sut.initFetchFor(index: 0)
        //test that data count equal expected number
        XCTAssertEqual(sut.modelForChart.dates.count, KeyConstants.Numbers.numberOfDaysForBiorythm)
        XCTAssertEqual(sut.modelForChart.days.count, KeyConstants.Numbers.numberOfDaysForBiorythm)
        XCTAssertEqual(sut.modelForChart.emotional.count, KeyConstants.Numbers.numberOfDaysForBiorythm)
        XCTAssertEqual(sut.modelForChart.physical.count, KeyConstants.Numbers.numberOfDaysForBiorythm)
        XCTAssertEqual(sut.modelForChart.spyrital.count, KeyConstants.Numbers.numberOfDaysForBiorythm)
    }
    
    func testFetchingDataForTomorrowCharts() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        sut = BiorythmViewModel()
        sut.initFetchFor(index: 1)
        //test that data count equal expected number
        XCTAssertEqual(sut.modelForChart.dates.count, KeyConstants.Numbers.numberOfDaysForBiorythm)
        XCTAssertEqual(sut.modelForChart.days.count, KeyConstants.Numbers.numberOfDaysForBiorythm)
        XCTAssertEqual(sut.modelForChart.emotional.count, KeyConstants.Numbers.numberOfDaysForBiorythm)
        XCTAssertEqual(sut.modelForChart.physical.count, KeyConstants.Numbers.numberOfDaysForBiorythm)
        XCTAssertEqual(sut.modelForChart.spyrital.count, KeyConstants.Numbers.numberOfDaysForBiorythm)
    }

}
